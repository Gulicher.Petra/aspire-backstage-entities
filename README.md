# Generating Backstage Entities from ASPIRE data

## Why

We catalogue the data that drives our [Service Catalogue (ASPIRE)](https://ab.co/aspire) in an Excel document, but even Excel has its limits when it comes to maintaining quality, up-to-date and versioned data.

Enter [Spotify's Backstage](https://backstage.spotify.com), which offers:

> * Software Catalog: All your services, websites, libraries, and data in one place.
> * Software Templates: Make sure the right way to build is also the easiest way.
> * Powerful plugins: Our Soundcheck plugin comes preinstalled. Plus, TechDocs and more.
> * No-code setup: PJust step through the onboarding wizards to set up Portal.

## The data model

ASPIRE records attributes that describe:

1. Services: Components, API's & Resources that form our software footprint, also:
    - The organisation's groups (teams) that operate the services
    - The organisation's domains (divisions?) that organise around the capabilities that services offer
2. Edges: Connections & dependencies between Services that make our software hum

To represent ASPIRE's taxonomy in Backstage we need to create the following entities:

```
┌─────────────────────────────────────────────────┐
│   Groups (Groups xXX)                           │
└─────────────────────────────────────────────────┘ 
┌─────────────────────────────────────────────────┐
│   Domains (Domains x5)                          │
│   ┌─────────────────────────────────────────┐   │
│   │   Systems (or Sub-Domains) (Models x8)  │   │
│   │   ┌─────────────────────────────────┐   │   │
│   │   │                                 │   │   │
│   │   │   Components (Services x1305)   │   │   │
│   │   │                                 │   │   │
│   │   └─────────────────────────────────┘   │   │
│   └─────────────────────────────────────────┘   │
└─────────────────────────────────────────────────┘ 
```

## Processing steps

To steps to prepare data for import into Backstage are:

1. Export from Excel to CSV
    - Selecting columns from the `Services` tabs: Service, Domain, Model, Group, Q2 25
    - With column data mapped to: `['title', 'domain', 'model', 'group', 'lifecycle']`
    - Saved in this repo at `./input/services.csv`

2. Create YAML Entity objects

In `./index.js` you'll find the definitions of `Group`, `Domain`, `System` & `Service` entities.

New entity types & fields can be added to ensure all ASPIRE fields are represented. 

3. Save YAML Entities to file

Running the following command will run the transformation job:

```yarn start```

Resulting in an output file called `entities.yaml`, which is ready to be ingested by Backstage.