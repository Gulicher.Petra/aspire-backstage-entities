const yaml = require('yaml')
const csv = require('csv-parse/sync')

const _ = require('lodash')

const fs = require('node:fs')

// const OUTPUT_DIR = "/Users/gulicher.petra/git/backstage/examples"
const OUTPUT_DIR = __dirname + "/output"

function parseEntities(input) {
    const data = fs.readFileSync(__dirname + input, 'utf8')

    const parsed = csv.parse(data, { columns: ['title', 'domain', 'model', 'group', 'lifecycle'], from: 2 })

    const groups = parsed.map(({ group }) => ({
        apiVersion: "backstage.io/v1alpha1",
        kind: "Group",
        metadata: {
            name: _.kebabCase(group).replace(/[^0-9a-z-]/gi, ''),
            title: group
        },
        spec: {
            type: "team",
            children: []
        }
    }))
    
    const domains = _.uniqBy(parsed, i => i.domain).map(({ group, domain }) => ({
        apiVersion: "backstage.io/v1alpha1",
        kind: "Domain",
        metadata: {
            name: _.kebabCase(domain),
            title: domain
        },
        spec: {
            owner: "group:" + _.kebabCase(group).replace(/[^0-9a-z-]/gi, ''),
        }
    }))
    const systems = _.uniqBy(parsed, i => i.model).map(({ model, group, domain }) => ({
        apiVersion: "backstage.io/v1alpha1",
        kind: "System",
        metadata: {
            name: _.kebabCase(model),
            title: model
        },
        spec: {
            owner: "group:" + _.kebabCase(group).replace(/[^0-9a-z-]/gi, ''),
            domain: "domain:" + _.kebabCase(domain).replace(/[^0-9a-z-]/gi, '')
        }
    }))
    const services = parsed.map(({ title, model, lifecycle, domain, group }) => ({
        apiVersion: "backstage.io/v1alpha1",
        kind: "Component",
        metadata: {
            name: _.kebabCase(title),
            title
        },
        spec: {
            type: "service",
            lifecycle: lifecycle || "Unknown",
            owner: "group:" + _.kebabCase(group).replace(/[^0-9a-z-]/gi, ''),
            system: "system:" + _.kebabCase(model).replace(/[^0-9a-z-]/gi, ''),
            domain: "domain:" + _.kebabCase(domain).replace(/[^0-9a-z-]/gi, '')
        }
    }))

    const output = groups.map(entity => yaml.stringify(entity)).join('---\n')
        + "---\n"
        + domains.map(entity => yaml.stringify(entity)).join('---\n')
        + "---\n"
        + systems.map(entity => yaml.stringify(entity)).join('---\n')
        + "---\n"
        + services.map(entity => yaml.stringify(entity)).join('---\n')

    fs.writeFileSync(OUTPUT_DIR + '/entities.yaml', "---\n" + output)
}

parseEntities('/input/services.csv')
